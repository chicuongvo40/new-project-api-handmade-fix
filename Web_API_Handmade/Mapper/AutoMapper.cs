﻿using AutoMapper;
using BusinessObjects.Models;
using DataService.DataTransfer;
using System.Reflection.Metadata;

namespace Web_API_Handmade.Mapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            #region Category
            CreateMap<Category, CategoryRequest>().ReverseMap();
            CreateMap<CategoryRequest, Category>().ReverseMap();
            CreateMap<UpdateCatagoryRequest, Category>().ReverseMap();
            #endregion

            #region Customer
            CreateMap<Customer, CustomerRequest>().ReverseMap();
            CreateMap<CustomerRequest, Customer>();
            CreateMap<UpdateCustomerRequest, Customer>();
            CreateMap<CreateCustomerWebRequest, Customer>();
            #endregion

            #region Order
            CreateMap<Order, OrderRequest>().ReverseMap();
            CreateMap<OrderDetail, OrderDetailRequest>().ReverseMap();
            CreateMap<OrderDetailRequest, OrderDetail>();
            CreateMap<CreateOrderRequest, Order>();
            CreateMap<CreateOrderRequest, Order>()
            .ForMember(c => c.Customer, option => option.Ignore());

            #endregion

            #region Product
            CreateMap<Product, ProductRequest>().ReverseMap();
            CreateMap<ProductRequest, Product>();
            CreateMap<UpdateProductRequest, Product>();
            #endregion

            #region RequestCustomer
            CreateMap<RequestCustomer, RequestCustomer>().ReverseMap();
            #endregion
        }
    }
}

