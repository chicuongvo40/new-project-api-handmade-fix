﻿using DataService.DataTransfer;
using DataService.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Web_API_Handmade.Controllers
{
    //[Authorize(Roles = "Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }


        /// <summary>
        /// Get List Category
        /// </summary>
        /// <param name="paging"></param>
        /// <returns></returns>
        [HttpGet("GetListCategory")]
        public async Task<ActionResult<PagedResults<CategoryRequest>>> GetListCategory([FromQuery] PagingRequest paging)
        {
            try
            {
                var rs = await _categoryService.GetListCategory(paging);
                return Ok(rs);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [HttpPost("CreateCategory")]
        public async Task<ActionResult<CategoryRequest>> CreateCatagory([FromBody] CategoryRequest request)
        {
            var rs = await _categoryService.CreateCatagory(request);
            return Ok(rs);
        }
        [HttpDelete("DeleteCategory")]
        public async Task<ActionResult<CategoryRequest>> UpdateProduct([FromQuery] int categoryId)
        {
            var rs = await _categoryService.DeleteCatagory(categoryId);
            return Ok(rs);
        }
        [HttpPut("UpdateProduct")]
        public async Task<ActionResult<CategoryRequest>> UpdateProduct([FromQuery] int categoryId, [FromBody] UpdateCatagoryRequest request)
        {
            var rs = await _categoryService.UpdateCatagory(categoryId, request);
            return Ok(rs);
        }
    }
}
