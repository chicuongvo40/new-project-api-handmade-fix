﻿using DataService.DataTransfer;
using DataService.Interface;
using Microsoft.AspNetCore.Mvc;

namespace Web_API_Handmade.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : Controller
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }
        /// <summary>
        /// Get Customer
        /// </summary>
        /// <param name="request"></param>
        /// <param name="paging"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<PagedResults<CustomerRequest>>> GetCustomers([FromQuery] CustomerRequest request, [FromQuery] PagingRequest paging)
        {
            var rs = await _customerService.GetCustomers(request, paging);
            return Ok(rs);
        }
        [HttpGet("FindCustomer")]
        public async Task<ActionResult<CustomerRequest>> FindCustomer([FromQuery] CustomerRequest request)
        {
            var rs = await _customerService.FindCustomer(request);
            return Ok(rs);
        }
        [HttpGet("FindCustomerByEmail")]
        public async Task<ActionResult<CustomerRequest>> FindCustomerByEmail([FromQuery] string email)
        {
            var rs = await _customerService.GetCustomerByEmail(email);
            return Ok(rs);
        }
        [HttpPost("CreateCustomer")]
        public async Task<ActionResult<CustomerRequest>> CreateCustomer([FromBody] CustomerRequest request)
        {
            var rs = await _customerService.CreateCustomer(request);
            return Ok(rs);
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<CustomerRequest>> UpdateCustomer(int id, [FromBody] UpdateCustomerRequest request)
        {
            var rs = await _customerService.UpdateCustomer(id, request);
            return Ok(rs);
        }
        [HttpPost("CreateCustomerWeb")]
        public async Task<ActionResult<CustomerRequest>> CreateCustomerWeb([FromBody] CreateCustomerWebRequest request)
        {
            var rs = await _customerService.CreateCustomerWeb(request);
            return Ok(rs);
        }
    }
}
