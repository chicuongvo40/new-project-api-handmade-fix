﻿using BusinessObjects.Models;
using DataService.DataTransfer;
using DataService.Interface;
using DataService.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Web_API_Handmade.Controllers
{
    //[Authorize(Roles = "Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class RequestCustomerController : Controller
    {
        private readonly IRequestCustomerService _requestCustomerService;
        public RequestCustomerController(IRequestCustomerService requestCustomerService)
        {
            _requestCustomerService = requestCustomerService;
        }
        /// <summary>
        /// Get List Category
        /// </summary>
        /// <param name="paging"></param>
        /// <returns></returns>
        [HttpGet("GetRequestCustomer")]
        public async Task<ActionResult<PagedResults<RequestCustomer>>> GetRequestCustomer([FromQuery] PagingRequest paging)
        {
            try
            {
                var rs = await _requestCustomerService.GetRequestCustomer(paging);
                return Ok(rs);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [HttpPost("CreateRequestCustomer")]
        public async Task<ActionResult<RequestCustomer>> CreateRequestCustomer([FromBody] RequestCustomer request)
        {
            var rs = await _requestCustomerService.CreateRequestCustomer(request);
            return Ok(rs);
        }

       
    }
}
