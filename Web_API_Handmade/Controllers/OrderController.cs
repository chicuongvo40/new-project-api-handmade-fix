﻿using BusinessObjects.Models;
using ClosedXML.Excel;
using DataService.DataTransfer;
using DataService.Interface;
using DataService.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using static DataService.Helpers.Enum;

namespace Web_API_Handmade.Controllers
{
    //[Authorize(Roles = "Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : Controller
    {
        private readonly IOrderService _orderService;
        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;

        }
        /// <summary>
        /// Get Orders
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<PagedResults<Order>>> GetOrders([FromQuery] PagingRequest paging)
        {
            try
            {
                var rs = await _orderService.GetOrders(paging);
                return Ok(rs);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// Get List Orders By Order Status
        /// </summary>
        /// <param name="paging"></param>
        /// <returns></returns>
        [HttpGet("GetListOrderByOrderStatus")]
        public async Task<ActionResult<PagedResults<Order>>> GetOrderByOrderStatus([FromQuery] OrderStatusEnum orderStatus, [FromQuery] int customerId, [FromQuery] PagingRequest paging)
        {
            try
            {
                var rs = await _orderService.GetOrderByOrderStatus(orderStatus, customerId, paging);
                return Ok(rs);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// Get Order By Id
        /// </summary>
        [HttpGet("{Id}")]
        public async Task<ActionResult<PagedResults<Order>>> GetOrderById(int Id)
        {
            try
            {
                var rs = await _orderService.GetOrderById(Id);
                return Ok(rs);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("CreateOrder")]
        public async Task<ActionResult<Order>> CreateOrder([FromBody] OrderRequest request)
        {
            try
            {
                var result = await _orderService.CreateOrder(request);
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Update Order 
        /// </summary>

        [HttpPut("{orderId}")]
        public async Task<ActionResult<Order>> UpdateOrderStatus(int orderId, OrderStatusEnum orderStatus)
        {
            try
            {
                var rs = await _orderService.UpdateOrderStatus(orderId, orderStatus);
                return Ok(rs);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("ExportExcel")]
        public async Task<ActionResult> ExportExcel()
        {
            DataTable dt = new DataTable();
            dt.TableName = "Empdata";
            dt.Columns.Add("Code", typeof(int));
            dt.Columns.Add("Name", typeof(string));
            
            dt.Columns.Add("Phone", typeof(string));
            dt.Columns.Add("FinalAmount", typeof(string));
            dt.Columns.Add("Date", typeof(string));

            var _list = await _orderService.GetAllOrder();

            if (_list.Count > 0)
                _list = _list.OrderByDescending(p => p.Id).ToList();
            {
                int a = 0;
                _list.ForEach(item =>
                {
                    dt.Rows.Add(++a, item.OrderName, item.DeliveryPhone, item.FinalAmount, item.CheckInDate);
                });
            }

            using (XLWorkbook wb = new XLWorkbook())
            {
                var sheet1 = wb.AddWorksheet(dt, "Employee Records");

                sheet1.Column(1).Style.Font.FontColor = XLColor.Red;

                sheet1.Columns(2, 4).Style.Font.FontColor = XLColor.Blue;

                sheet1.Row(1).CellsUsed().Style.Fill.BackgroundColor = XLColor.Black;
                //sheet1.Row(1).Cells(1,3).Style.Fill.BackgroundColor = XLColor.Yellow;
                sheet1.Row(1).Style.Font.FontColor = XLColor.White;

                sheet1.Row(1).Style.Font.Bold = true;
                sheet1.Row(1).Style.Font.Shadow = true;
                sheet1.Row(1).Style.Font.Underline = XLFontUnderlineValues.Single;
                sheet1.Row(1).Style.Font.VerticalAlignment = XLFontVerticalTextAlignmentValues.Superscript;
                sheet1.Row(1).Style.Font.Italic = true;

                sheet1.Rows(2, 3).Style.Font.FontColor = XLColor.AshGrey;

                using (MemoryStream ms = new MemoryStream())
                {
                    wb.SaveAs(ms);
                    return File(ms.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Sample.xlsx");
                }
            }
            return Ok();

        }
    }
}

