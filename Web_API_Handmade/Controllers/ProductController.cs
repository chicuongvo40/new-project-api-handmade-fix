﻿using BusinessObjects.Models;
using DataService.DataTransfer;
using DataService.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.NetworkInformation;

namespace Web_API_Handmade.Controllers
{
    //[Authorize(Roles = "Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : Controller
    {
        private readonly IProductServices _productService;

        public ProductController(IProductServices productServices)
        {
            _productService = productServices;
        }

        /// <summary>
        /// Get List Product
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<PagedResults<ProductRequest>>> GetProducts([FromQuery] PagingRequest paging)
        {
            try
            {
                var rs = await _productService.GetProducts(paging);
                return Ok(rs);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Get Product By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetById")]
        public async Task<ActionResult<ProductRequest>> GetProductById([FromQuery] int id)
        {
            var rs = await _productService.GetProductById(id);
            return Ok(rs);
        }
        /// <summary>
        /// Get Product By Store
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet("GetProductByRating")]
        public async Task<ActionResult<PagedResults<ProductRequest>>> GetProductByCode([FromQuery] string rating)
        {
            var rs = await _productService.GetProductByRating(rating);
            return Ok(rs);
        }
        /// <summary>
        /// Get Product By Category
        /// </summary>
        /// <param name="cateId"></param>
        /// <returns></returns>
        [HttpGet("GetProductByCategory")]
        public async Task<ActionResult<PagedResults<ProductRequest>>> GetProductByCategory([FromQuery] int cateId, [FromQuery] PagingRequest paging)
        {
            var rs = await _productService.GetProductByCategory(cateId, paging);
            return Ok(rs);
        }
        /// <summary>
        /// Create Product
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("CreateProduct")]
        public async Task<ActionResult<ProductRequest>> CreateProduct([FromBody] ProductRequest request)
        {
            var rs = await _productService.CreateProduct(request);
            return Ok(rs);
        }
        /// <summary>
        /// Update Product
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("UpdateProduct")]
        public async Task<ActionResult<ProductRequest>> UpdateProduct([FromQuery] int productId, [FromBody] UpdateProductRequest request)
        {
            var rs = await _productService.UpdateProduct(productId, request);
            return Ok(rs);
        }

        /// <summary>
        /// Search Product
        /// </summary>
        /// <param name="searchString"></param>
        /// <param name="timeSlotId"></param>
        /// <returns></returns>
        [HttpGet("SearchProduct")]
        public async Task<ActionResult<PagedResults<ProductRequest>>> SearchProduct([FromQuery] string searchString, [FromQuery] int cateId, [FromQuery] PagingRequest paging)
        {
            var rs = await _productService.SearchProduct(searchString, cateId, paging);
            return Ok(rs);
        }

        /// <summary>
        /// Delete Product Menu
        /// </summary>
        /// <param name="productInMenuId"></param>
        /// <returns></returns>
        [HttpDelete("DeleteProduct")]
        public async Task<ActionResult<ProductRequest>> UpdateProduct([FromQuery] int productId)
        {
            var rs = await _productService.DeleteProduct(productId);
            return Ok(rs);
        }
    }
}