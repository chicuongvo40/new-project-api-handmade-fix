﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.DataTransfer
{
    public class OrderDetailRequest
    {   
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public double FinalAmount { get; set; }
    }
}
