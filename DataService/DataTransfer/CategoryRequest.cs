﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.DataTransfer
{
    public class CategoryRequest
    {
        public int Id { get; set; }
        public string CategoryName { get; set; } 
        public int? Status { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime? UpdateAt { get; set; }
        public string? Description { get; set; }
    }
}
