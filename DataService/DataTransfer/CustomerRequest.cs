﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.DataTransfer
{
    public class CustomerRequest
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Phone { get; set; } 
        public string? Email { get; set; }
        public string? Password { get; set; }
    }
}
