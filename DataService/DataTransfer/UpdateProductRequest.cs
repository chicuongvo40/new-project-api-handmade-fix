﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.DataTransfer
{
    public class UpdateProductRequest
    {
        public string? Name { get; set; }
        public string? Image { get; set; }
        public double Price { get; set; }
        public string? Detail { get; set; }
        public int Status { get; set; }
        public int CategoryId { get; set; }
        public int Quantity { get; set; }
        public string Rating { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
