﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.DataTransfer
{
    public class CreateCustomerWebRequest
    {
        public string Name { get; set; }
        public string? Password { get; set; }
    }
}
