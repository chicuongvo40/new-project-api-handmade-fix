﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.DataTransfer
{
    public class CreateOrderRequest
    {
        public string? Address { get; set; }
        public double TotalAmount { get; set; }
        public int OrderStatus { get; set; }
        public int CustomerId { get; set; }
        public string? DeliveryPhone { get; set; }
        public int OrderType { get; set; }
        public virtual ICollection<OrderDetailRequest> OrderDetails { get; set; }
    }
}
