﻿using DataService.DataTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.Interface
{
    public interface ICategoryService
    {
        Task<CategoryRequest> CreateCatagory(CategoryRequest request);
        Task<PagedResults<CategoryRequest>> GetListCategory(PagingRequest paging);

        Task<CategoryRequest> DeleteCatagory(int cateId);

        Task<CategoryRequest> UpdateCatagory(int cateId, UpdateCatagoryRequest request);
    }
}
