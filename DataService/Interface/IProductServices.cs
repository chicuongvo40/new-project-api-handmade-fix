﻿using BusinessObjects.Models;
using DataService.DataTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.Interface
{
    public interface IProductServices
    {
        Task<PagedResults<ProductRequest>> GetProducts(PagingRequest paging);
        Task<ProductRequest> GetProductById(int productId);
        Task<ProductRequest> GetProductByRating(string rating);
        //Task<PagedResults<ProductRequest>> GetProductByStore(int storeId, PagingRequest paging);
        Task<PagedResults<ProductRequest>> GetProductByCategory(int cateId, PagingRequest paging);
        //Task<PagedResults<ProductRequest>> GetProductByTimeSlot(int timeSlotId, PagingRequest paging);
        Task<ProductRequest> CreateProduct(ProductRequest request);
        Task<ProductRequest> UpdateProduct(int productId, UpdateProductRequest request);
        Task<PagedResults<ProductRequest>> SearchProduct(string searchString, int cateId, PagingRequest paging);
        //Task<PagedResults<ProductRequest>> SearchProductInMenu(string searchString, int timeSlotId, PagingRequest paging);
        Task<ProductRequest> DeleteProduct(int productId);
    }
}
