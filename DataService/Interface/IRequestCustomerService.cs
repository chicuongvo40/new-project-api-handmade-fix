﻿using BusinessObjects.Models;
using DataService.DataTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.Interface
{
    public interface IRequestCustomerService
    {
        Task<PagedResults<RequestCustomer>> GetRequestCustomer(PagingRequest paging);

        Task<RequestCustomer> CreateRequestCustomer(RequestCustomer request);
    }
}
