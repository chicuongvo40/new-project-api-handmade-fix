﻿using DataService.DataTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.Interface
{
    public interface ICustomerService
    {
        Task<PagedResults<CustomerRequest>> GetCustomers(CustomerRequest request, PagingRequest paging);

        Task<CustomerRequest> CreateCustomer(CustomerRequest request);
        Task<CustomerRequest> GetCustomerByEmail(string email);
        Task<CustomerRequest> UpdateCustomer(int customerId, UpdateCustomerRequest request);
        Task<CustomerRequest> CreateCustomerWeb(CreateCustomerWebRequest request);
        Task<CustomerRequest> FindCustomer(CustomerRequest request);
    }
}
