﻿using BusinessObjects.Models;
using DataService.DataTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DataService.Helpers.Enum;

namespace DataService.Interface
{
    public interface IOrderService
    {
        Task<List<Order>> GetAllOrder();
        Task<PagedResults<Order>> GetOrders(PagingRequest paging);
        Task<Order> GetOrderById(int orderId);
        Task<PagedResults<Order>> GetOrderByOrderStatus(OrderStatusEnum orderStatus, int customerId, PagingRequest paging);
        Task<Order> CreateOrder(OrderRequest request);
        Task<Order> UpdateOrderStatus(int orderId, OrderStatusEnum orderStatus);
    }
}
