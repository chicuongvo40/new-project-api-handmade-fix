﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using BusinessObjects.Models;
using BusinessObjects.UniOfWork;
using DataService.DataTransfer;
using DataService.Exceptions;
using DataService.Helpers;
using DataService.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace DataService.Service
{
    public class ProductServices : IProductServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ProductServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<ProductRequest> CreateProduct(ProductRequest request)
        {
            try
            {
                var product = _mapper.Map<ProductRequest, Product>(request);

                product.Name = request.Name;
                product.Image = request.Image;
                product.Price = request.Price;
                product.Detail = request.Detail;
                product.Status = request.Status;
                product.CategoryId = request.CategoryId;
                product.Rating = request.Rating;
                product.Quantity = request.Quantity;
                product.CreateAt = DateTime.Now;
                product.UpdatedAt = request.UpdatedAt;

                await _unitOfWork.Repository<Product>().InsertAsync(product);
                await _unitOfWork.CommitAsync();

                return _mapper.Map<Product, ProductRequest>(product);
            }
            catch (CrudException ex)
            {
                throw new CrudException(ex.Status, ex.Message, ex?.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public async Task<ProductRequest> DeleteProduct(int productId)
        {
            try
            {
                Product product = null;
                product = _unitOfWork.Repository<Product>()
                    .Find(p => p.Id == productId);
                if (product == null)
                {
                    throw new CrudException(HttpStatusCode.NotFound, "Not found Catagory with id", "a");
                }
                product.Status = 2;
                product.UpdatedAt = DateTime.Now;
                await _unitOfWork.Repository<Product>().UpdateDetached(product);
                await _unitOfWork.CommitAsync();
                return _mapper.Map<Product, ProductRequest>(product);
            }
            catch (Exception ex)
            {
                throw new CrudException(HttpStatusCode.BadRequest, "Delete product error!!!!", ex?.Message);
            }
        }

        public async Task<PagedResults<ProductRequest>> GetProductByCategory(int cateId, PagingRequest paging)
        {
            try
            {
                var product = await _unitOfWork.Repository<Product>().GetAll()
                                .Where(x => x.CategoryId == cateId)
                                .ProjectTo<ProductRequest>(_mapper.ConfigurationProvider)
                                .ToListAsync();

                var result = PageHelper<ProductRequest>.Paging(product, paging.Page, paging.PageSize);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<ProductRequest> GetProductByRating(string rating)
        {
            Product product = null;
            product = await _unitOfWork.Repository<Product>().GetAll()
                .Where(x => x.Rating.Contains(rating))
                .FirstOrDefaultAsync();

            if (product == null)
            {
                throw new CrudException(HttpStatusCode.NotFound, "Not found product with code", rating);
            }
            return _mapper.Map<Product, ProductRequest>(product);
        }

        public async Task<ProductRequest> GetProductById(int productId)
        {
            Product product = null;
            product = await _unitOfWork.Repository<Product>().GetAll()
                .Where(x => x.Id == productId)
                .FirstOrDefaultAsync();

            if (product == null)
            {
                throw new CrudException(HttpStatusCode.NotFound, "Not found product with id", productId.ToString());
            }
            return _mapper.Map<Product, ProductRequest>(product);
        }

        public async Task<PagedResults<ProductRequest>> GetProducts(PagingRequest paging)
        {
            try
            {
                var product = _unitOfWork.Repository<Product>().GetAll().ToArray();
                var productList = _mapper.Map<Product[], ProductRequest[]>(product).ToList();
                List<ProductRequest> list1 = new List<ProductRequest>();
                foreach (var item in productList)
                {
                    list1.Add(item);
                }
                var result = PageHelper<ProductRequest>.Paging(list1, paging.Page, paging.PageSize);
                return result;
            }
            catch (CrudException ex)
            {
                throw new CrudException(HttpStatusCode.BadRequest, "Get Menu Error!!!!", ex?.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }

        public async Task<PagedResults<ProductRequest>> SearchProduct(string searchString, int cateId, PagingRequest paging)
        {
            var product = _unitOfWork.Repository<Product>().GetAll()
                                .Where(x => x.Name.Contains(searchString))
                                .ProjectTo<ProductRequest>(_mapper.ConfigurationProvider)
                                .ToList();

            var result = PageHelper<ProductRequest>.Paging(product, paging.Page, paging.PageSize);
            return result;
        }

        public async Task<ProductRequest> UpdateProduct(int productId, UpdateProductRequest request)
        {
            try
            {
                Product product = null;
                product = _unitOfWork.Repository<Product>()
                    .Find(p => p.Id == productId);
                if (product == null)
                {
                    throw new CrudException(HttpStatusCode.NotFound, "Not found product with id", productId.ToString());
                }
                _mapper.Map<UpdateProductRequest, Product>(request, product);
                product.Status = request.Status;
                product.UpdatedAt = DateTime.Now;

                await _unitOfWork.Repository<Product>().UpdateDetached(product);
                await _unitOfWork.CommitAsync();
                return _mapper.Map<Product, ProductRequest>(product);
            }
            catch (Exception ex)
            {
                throw new CrudException(HttpStatusCode.BadRequest, "Update product error!!!!", ex?.Message);
            }
        }
    }
}
