﻿using AutoMapper;
using BusinessObjects.Models;
using BusinessObjects.UniOfWork;
using DataService.DataTransfer;
using DataService.Exceptions;
using DataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static DataService.Helpers.Enum;
using static System.Formats.Asn1.AsnWriter;
using System.Text.RegularExpressions;
using Microsoft.EntityFrameworkCore;
using IronBarCode;
using SixLabors.ImageSharp.Drawing;
using System.Diagnostics;
using DataService.Helpers;

namespace DataService.Service
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public OrderService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Order> CreateOrder(OrderRequest request)
        {
         
            #region checkDeliveryPhone
            var check = CheckVNPhoneEmail(request.DeliveryPhone);
            if (check)
            {
                if (!request.DeliveryPhone.StartsWith("+84"))
                {
                    request.DeliveryPhone = request.DeliveryPhone.TrimStart(new char[] { '0' });
                    request.DeliveryPhone = "+84" + request.DeliveryPhone;
                }
            }
            else
            {
                throw new CrudException(HttpStatusCode.BadRequest, "Wrong Phone", request.DeliveryPhone.ToString());
            }
            #endregion
            Order order = new Order();
            #region OrderName
            string refixOrderName = "HD";
            order.OrderName = refixOrderName + "-" + "a";
            #endregion
            order.Customer = _unitOfWork.Repository<Customer>().Find(x => x.Id == request.CustomerId);
            order.Address = request.Address;
            order.CheckInDate = DateTime.Now;
            order.OrderStatus = (int)OrderStatusEnum.Pending;
            order.OrderType = request.OrderType;
            order.DeliveryPhone = request.DeliveryPhone;
            order.CustomerId = request.CustomerId;
            #region Order detail
            order.TotalAmount = request.TotalAmount;
            order.FinalAmount = request.TotalAmount;
            List<OrderDetail> listOrderDetail = new List<OrderDetail>();
            foreach (var detail in request.OrderDetails)
            {
                OrderDetail orderDetail = new OrderDetail();
                var product = _unitOfWork.Repository<Product>().GetAll()
                                               .Where(x => x.Id == detail.ProductId)
                                    .FirstOrDefault();
                orderDetail.ProductName = product.Name;
                orderDetail.Quantity = detail.Quantity;
                orderDetail.FinalAmount = (double)request.FinalAmount;
                orderDetail.Status = 1;
                orderDetail.ProductId = detail.ProductId;
                listOrderDetail.Add(orderDetail);
            }
            #endregion
            order.OrderDetails = listOrderDetail;
            await _unitOfWork.Repository<Order>().InsertAsync(order);
                await _unitOfWork.CommitAsync();
                return order;
        }
    public static bool CheckVNPhoneEmail(string phoneNumber)
        {
            string strRegex = @"(^(0)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$)";
            Regex re = new Regex(strRegex);
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");

            if (re.IsMatch(phoneNumber))
            {
                return true;
            }
            else
                return false;
        }
        //public Task<List<OrderRequest>> CreateOrder(CreateOrderRequest request)
        //{
        //    throw new NotImplementedException();
        //}

        public async Task<PagedResults<Order>> GetOrders(PagingRequest paging)
        {
            try
            {
                var orderList = _unitOfWork.Repository<Order>().GetAll()
                                 .ToList();

                List<Order> result = new List<Order>();
                foreach (var order in orderList)
                {
                    var orderDetail = _mapper.Map<List<OrderDetail>>(order.OrderDetails);
                    var customerResult = _mapper.Map<Customer, Customer>(order.Customer);
                    var orderResult = new Order()
                    {
                        Id = order.Id,
                        OrderName = order.OrderName,
                        Address = order.Address,
                        CheckInDate = order.CheckInDate,
                        TotalAmount = order.TotalAmount,
                        FinalAmount = order.FinalAmount,
                        OrderStatus = order.OrderStatus,
                        DeliveryPhone = order.DeliveryPhone,
                        OrderType = order.OrderType,
                        Customer = customerResult,
                        OrderDetails = orderDetail
                    };
                    result.Add(orderResult);
                }
                return PageHelper<Order>.Paging(result, paging.Page, paging.PageSize);
            }
            catch (Exception ex)
            {
                throw new CrudException(HttpStatusCode.BadRequest, "Error", ex.Message);
            }
        }
        public async Task<PagedResults<Order>> GetOrderByOrderStatus(OrderStatusEnum orderStatus, int customerId, PagingRequest paging)
        {
            try
            {
                var orderList = _unitOfWork.Repository<Order>().GetAll()
                            .Where(x => x.OrderStatus == (int)orderStatus
                            )
                            .ToList();

                List<Order> result = new List<Order>();
                foreach (var order in orderList)
                {
                    var orderDetail = _mapper.Map<List<OrderDetail>>(order.OrderDetails);
                    var customerResult = _mapper.Map<Customer, Customer>(order.Customer);
                    var orderResult = new Order()
                    {
                        Id = order.Id,
                        OrderName = order.OrderName,
                        Address = order.Address,
                        CheckInDate = order.CheckInDate,
                        TotalAmount = order.TotalAmount,
                        FinalAmount = order.FinalAmount,
                        OrderStatus = order.OrderStatus,
                        DeliveryPhone = order.DeliveryPhone,
                        OrderType = order.OrderType,
                        CustomerId = order.CustomerId,
                        Customer = customerResult,
                        OrderDetails = orderDetail
                    };
                    result.Add(orderResult);
                }
                return PageHelper<Order>.Paging(result, paging.Page, paging.PageSize);
            }
            catch (Exception ex)
            {
                throw new CrudException(HttpStatusCode.BadRequest, "Error", ex.Message);
            }
        }
        public async Task<Order> GetOrderById(int orderId)
        {
            try
            {
                var order = _unitOfWork.Repository<Order>().Find(x => x.Id == orderId);

                if (order == null)
                    throw new CrudException(HttpStatusCode.NotFound, "Order Id not found", orderId.ToString());
                var customerResult = _mapper.Map<Customer, Customer>(order.Customer);
                var orderResult = _mapper.Map<List<OrderDetail>>(order.OrderDetails);


                return new Order()
                {
                   Id = order.Id,
                        OrderName = order.OrderName,
                        Address = order.Address,
                        CheckInDate = order.CheckInDate,
                        TotalAmount = order.TotalAmount,
                        FinalAmount = order.FinalAmount,
                        OrderStatus = order.OrderStatus,
                        DeliveryPhone = order.DeliveryPhone,
                        OrderType = order.OrderType,
                        CustomerId = order.CustomerId,
                        Customer = customerResult,
                        OrderDetails = orderResult
                };
            }
            catch (CrudException e)
            {
                throw e;
            }
            catch (Exception ex)
            {
                throw new CrudException(HttpStatusCode.BadRequest, "Error", ex.Message);
            }
        }
        public async Task<Order> UpdateOrderStatus(int orderId, OrderStatusEnum orderStatus)
        {
            try
            {
                var order = await _unitOfWork.Repository<Order>().GetAll()
                            .Where(x => x.Id == orderId)
                            .FirstOrDefaultAsync();
                order.OrderStatus = (int)orderStatus;

                await _unitOfWork.Repository<Order>().UpdateDetached(order);
                await _unitOfWork.CommitAsync();

                return _mapper.Map<Order>(order);
            }
            catch (Exception e)
            {
                throw new CrudException(HttpStatusCode.BadRequest, "Error", e.Message);
            }
        }
        public async Task<List<Order>> GetAllOrder()
        {

            var orderList = _unitOfWork.Repository<Order>().GetAll()
                             .ToList();

            List<Order> result = new List<Order>();
            foreach (var order in orderList)
            {
                var orderDetail = _mapper.Map<List<OrderDetail>>(order.OrderDetails);
                var customerResult = _mapper.Map<Customer, Customer>(order.Customer);
                var orderResult = new Order()
                {
                    Id = order.Id,
                    OrderName = order.OrderName,
                    Address = order.Address,
                    CheckInDate = order.CheckInDate,
                    TotalAmount = order.TotalAmount,
                    FinalAmount = order.FinalAmount,
                    OrderStatus = order.OrderStatus,
                    DeliveryPhone = order.DeliveryPhone,
                    OrderType = order.OrderType,
                    CustomerId = order.CustomerId,
                     Customer =  customerResult,
                    OrderDetails = orderDetail
                };
                result.Add(orderResult);
            }
            return result;
        }
    }
}
