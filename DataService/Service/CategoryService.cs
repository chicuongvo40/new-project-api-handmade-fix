﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using BusinessObjects.Models;
using BusinessObjects.UniOfWork;
using DataService.DataTransfer;
using DataService.Exceptions;
using DataService.Helpers;
using DataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DataService.Service
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CategoryService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<CategoryRequest> CreateCatagory(CategoryRequest request)
        {
            try
            {
                var checkProduct = _unitOfWork.Repository<Category>().Find(x => x.CategoryName.ToLower().Trim().Contains(request.CategoryName.ToLower().Trim()));
                if (checkProduct != null)
                {
                    throw new CrudException(HttpStatusCode.NotFound, "Category all ready exist!!!!!", request.CategoryName);
                }
                var product = _mapper.Map<CategoryRequest, Category>(request);

                product.Status = request.Status;
                product.CreateAt = DateTime.Now;
                product.CategoryName = request.CategoryName;
                product.Description = request.Description;
                product.UpdateAt = null;
                await _unitOfWork.Repository<Category>().InsertAsync(product);
                await _unitOfWork.CommitAsync();

                return _mapper.Map<Category, CategoryRequest>(product);
            }
            catch (CrudException ex)
            {
                throw new CrudException(ex.Status, ex.Message, ex?.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public async Task<CategoryRequest> DeleteCatagory(int cateId)
        {
            try
            {
                Category product = null;
                product = _unitOfWork.Repository<Category>()
                    .Find(p => p.Id == cateId);
                if (product == null)
                {
                    throw new CrudException(HttpStatusCode.NotFound, "Not found Catagory with id", "a");
                }
                product.Status = 2;
                product.UpdateAt = DateTime.Now;
                await _unitOfWork.Repository<Category>().UpdateDetached(product);
                await _unitOfWork.CommitAsync();
                return _mapper.Map<Category, CategoryRequest>(product);
            }
            catch (Exception ex)
            {
                throw new CrudException(HttpStatusCode.BadRequest, "Update product error!!!!", ex?.Message);
            }
        }

        public async Task<PagedResults<CategoryRequest>> GetListCategory(PagingRequest paging)
        {
            try
            {
                var list = _unitOfWork.Repository<Category>().GetAll()
                                .ProjectTo<CategoryRequest>(_mapper.ConfigurationProvider)
                                .ToList();
                List<CategoryRequest> list1 = new List<CategoryRequest>();

                foreach (var item in list)
                {
                    if (item.Status == 1)
                    {
                        list1.Add(item);
                    }

                }
                var result = PageHelper<CategoryRequest>.Paging(list1, paging.Page, paging.PageSize);

                return result;
            }
            catch (CrudException e)
            {
                throw new CrudException(HttpStatusCode.BadRequest, "Get category list error!!!!!", e.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<CategoryRequest> UpdateCatagory(int cateId, UpdateCatagoryRequest request)
        {
            try
            {
                Category product = null;
                product = _unitOfWork.Repository<Category>()
                    .Find(p => p.Id == cateId);
                if (product == null)
                {
                    throw new CrudException(HttpStatusCode.NotFound, "Not found Category with id", cateId.ToString());
                }
                _mapper.Map<UpdateCatagoryRequest, Category>(request, product);
                product.UpdateAt = DateTime.Now;
                await _unitOfWork.Repository<Category>().UpdateDetached(product);
                await _unitOfWork.CommitAsync();
                return _mapper.Map<Category, CategoryRequest>(product);
            }
            catch (Exception ex)
            {
                throw new CrudException(HttpStatusCode.BadRequest, "Update product error!!!!", ex?.Message);
            }
        }
    }
    }

