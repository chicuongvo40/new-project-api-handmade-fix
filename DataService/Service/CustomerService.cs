﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using BusinessObjects.Models;
using BusinessObjects.UniOfWork;
using DataService.DataTransfer;
using DataService.Exceptions;
using DataService.Helpers;
using DataService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DataService.Service
{
    public class CustomerService : ICustomerService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CustomerService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<CustomerRequest> CreateCustomer(CustomerRequest request)
        {
            try
            {

                var checkProduct = _unitOfWork.Repository<Customer>().Find(x => x.Name.ToLower().Trim().Contains(request.Name.ToLower().Trim()));
                if (checkProduct != null)
                {
                    throw new CrudException(HttpStatusCode.NotFound, "Customer all ready exist!!!!!", request.Name);
                }
                var customer = _mapper.Map<CustomerRequest, Customer>(request);
                customer.Name = request.Name;
                customer.Password = request.Password;
                customer.Email = request.Email;
                customer.Phone = request.Phone;
                await _unitOfWork.Repository<Customer>().InsertAsync(customer);
                await _unitOfWork.CommitAsync();

                return _mapper.Map<Customer, CustomerRequest>(customer);
            }
            catch (CrudException ex)
            {
                throw new CrudException(HttpStatusCode.BadRequest, "Create Customer Error!!!", ex?.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    

        public async Task<CustomerRequest> CreateCustomerWeb(CreateCustomerWebRequest request)
        {
            try
            {

                var checkProduct = _unitOfWork.Repository<Customer>().Find(x => x.Name.ToLower().Trim().Contains(request.Name.ToLower().Trim()));
                if (checkProduct != null)
                {
                    throw new CrudException(HttpStatusCode.NotFound, "Category all ready exist!!!!!", request.Name);
                }
                var customer = _mapper.Map<CreateCustomerWebRequest, Customer>(request);
                customer.Name = request.Name;
                customer.Password = request.Password;
                customer.Email = "";
                customer.Phone = "";
                await _unitOfWork.Repository<Customer>().InsertAsync(customer);
                await _unitOfWork.CommitAsync();

                return _mapper.Map<Customer, CustomerRequest>(customer);
            }
            catch (CrudException ex)
            {
                throw new CrudException(HttpStatusCode.BadRequest, "Create Product Error!!!", ex?.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public async Task<CustomerRequest> FindCustomer(CustomerRequest request)
        {
            try
            {

                var checkProduct = _unitOfWork.Repository<Customer>().Find(x => x.Name.ToLower().Trim().Contains(request.Name.ToLower().Trim()) && x.Password == request.Password);
                if (checkProduct == null)
                {
                    throw new CrudException(HttpStatusCode.NotFound, "Customer all ready exist!!!!!", request.Name);
                }
                var customer = _mapper.Map<CustomerRequest, Customer>(request);
                customer.Name = request.Name;
                customer.Password = request.Password;
                customer.Email ="";
                customer.Phone = "";
                return _mapper.Map<Customer, CustomerRequest>(customer);
            }
            catch (CrudException ex)
            {
                throw new CrudException(HttpStatusCode.BadRequest, "Find Customer Error!!!", ex?.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

       

        public async Task<CustomerRequest> GetCustomerByEmail(string email)
        {
            try
            {
                Customer customer = new Customer();
                customer = _unitOfWork.Repository<Customer>().GetAll()
                    .Where(x => x.Email.Contains(email)).FirstOrDefault();

                return _mapper.Map<Customer, CustomerRequest>(customer);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<PagedResults<CustomerRequest>> GetCustomers(CustomerRequest request, PagingRequest paging)
        {
            try
            {
                var customers = _unitOfWork.Repository<Customer>().GetAll()
                                           .ProjectTo<CustomerRequest>(_mapper.ConfigurationProvider)

                                           .ToList();
                var result = PageHelper<CustomerRequest>.Paging(customers, paging.Page, paging.PageSize);
                return result;
            }
            catch (CrudException ex)
            {
                throw new CrudException(HttpStatusCode.BadRequest, "Get customer list error!!!!!", ex.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public async Task<CustomerRequest> UpdateCustomer(int customerId, UpdateCustomerRequest request)
        {
            try
            {
                Customer customer = null;
                customer = _unitOfWork.Repository<Customer>()
                    .Find(c => c.Id == customerId);

                if (customer == null)
                {
                    throw new CrudException(HttpStatusCode.NotFound, "Not found customer with id", customerId.ToString());
                }

                _mapper.Map<UpdateCustomerRequest, Customer>(request, customer);

                await _unitOfWork.Repository<Customer>().UpdateDetached(customer);
                await _unitOfWork.CommitAsync();
                return _mapper.Map<Customer, CustomerRequest>(customer);
            }
            catch (CrudException ex)
            {
                throw new CrudException(HttpStatusCode.BadRequest, "Update customer error!!!!!", ex.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
