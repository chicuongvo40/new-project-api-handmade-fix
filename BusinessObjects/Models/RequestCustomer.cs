﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class RequestCustomer
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Phone { get; set; } = null!;
        public string Adress { get; set; } = null!;
        public DateTime Date { get; set; }
        public string Material { get; set; } = null!;
        public string Color { get; set; } = null!;
        public string Prices { get; set; } = null!;
        public string Description { get; set; } = null!;
        public string Img { get; set; } = null!;
    }
}
